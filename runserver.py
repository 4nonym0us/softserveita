from flask import Flask, jsonify, abort, make_response, url_for, render_template, request, redirect
from pymongo import MongoClient
from pprint import pprint
import time

app = Flask(__name__)

connect_string = "mongodb://user:test@kahana.mongohq.com:10075/blog_db"

def get_db():
    try:
        client = MongoClient(connect_string)
        return client.blog_db
    except:
        exit('Database connection failed')

@app.route("/index")
@app.route("/")
def index():
    all_posts = get_db().posts.find(sort=[("addeddate", -1)])
    return render_template('show_posts.html', 
                           posts=all_posts, 
                           show_header=True)

@app.route("/admin")
def admin():
    all_posts = get_db().posts.find(sort=[("addeddate", -1)])
    return render_template('admin.html', 
                           posts=all_posts, 
                           show_header=False)

@app.route('/post/add', methods=['GET'])
def get_post_editview():
    return render_template('add_post.html')

@app.route('/post/add', methods=['POST'])
def add_post():
    title = request.form['title']
    body = request.form['body']

    posts = get_db().posts
    try:
        post_id = posts.find_one(sort=[("id", -1)]).get('id') + 1
    except:
        post_id = 1

    posts.insert({
        "id": post_id,
        "title": title,
        "body": body,
        "description": body[:400] + '...' if body.__len__() > 400 else body,
        "addeddate": time.time()
        })

    return redirect(url_for('index'))
   

@app.route('/post/<int:postid>', methods=['GET'])
def get_post(postid):

    posts = get_db().posts
    post = posts.find_one({'id': postid})
   
    return render_template('view_post.html', post=post)

@app.route('/post/delete/<int:postid>', methods=['GET'])
def delete_post(postid):
    posts = get_db().posts
    posts.remove({'id': postid})
    #TODO: render_template() into proper html view
    #return 'Post was succesfully deleted.'
    return redirect(url_for('index'))

@app.route('/post/edit/<int:postid>', methods=['GET'])
def edit_post_view(postid):
    posts = get_db().posts
    post = posts.find_one({'id': postid})
    return render_template('edit_post.html', post=post)

@app.route('/post/edit/<int:postid>', methods=['POST'])
def edit_post(postid):
    posts = get_db().posts
    post = posts.find_one({'id': postid})
    posts.update(
        {'id': postid},
        {
            "id": postid,
            "title": request.form['title'],
            "body": request.form['body'],
            "description": body[:400] + '...' if request.form['body'].__len__() > 400 else request.form['body'],
            "addeddate": time.time(),
        }, True)
    post = posts.find_one({'id': postid})
    return render_template('view_post.html', post=post)

@app.errorhandler(404)
def page_not_found(error):
    return render_template('page_not_found.html'), 404

if __name__ == "__main__":
    app.run(debug=True)
