from flask import Flask, render_template, request
import sys
sys.path.append("/home/illidan/virtenv/modules")
from formysql import Usemysql

app = Flask(__name__)
@app.route("/main")
def selmain ():
  complete = dict()
  contents = []
  times = []
  ind = 0
  client = Usemysql("localhost", "root", "belear1994", "Bloguse")
  values = client.select("comments")
  for res1 in values:
    contents.append(res1[2])
    times.append(res1[3])
    ind = ind + 1
  for i in range(0, len(contents)):
    complete.update({i: contents[i]}) 
  return render_template("main.html", buflist = complete, buftime = times)

if __name__ == "__main__":
  app.run()
