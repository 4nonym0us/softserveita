#!/home/.../virtualenv/modules/formysql.py

import MySQLdb as usedb

class Usemysql:
#initializing copy of class Usemysql 
#meanings of arguments this function
#*args - list of values, which introduces parameters of connection with database
  def __init__ (self, *args):
    self.server = args[0]
    self.user = args[1]
    self.password = args[2]
    self.db = args[3]
  
#getting results of select request 
#meanings of arguments this function
#tablename - name of neccesary table 
  def select (self, tablename):
    rowsof = []
    try:
      connection1 = usedb.connect(self.server, self.user, self.password, self.db)
      cursor = connection1.cursor()
      cursor.execute("SELECT * FROM " + tablename)
      result1 = cursor.fetchall()
      for rows in result1:
        rowsof.append(rows)
      return rowsof
      connection1.close()
      cursor.close()
    except ConnectionError as e:
      msg = e
      all_msg = msg + " : connection denied"
      return all_msg
  
#adding of new post to database
#meanings of arguments this function
#tablename - name of neccesary table 
#args - list of new values, that creates new row in neccesary table
  def insert (self, tablename, args):
    try:
      connection1 = usedb.connect(self.server, self.user, self.password, self.db)
      cursor = connection1.cursor()
      requeststring = "INSERT INTO "+tablename+" VALUES("
      items = ""
      for ready in args:
        items = items + "'"+ready+"' ,"
      neccesary = items.split()
      length_of = len(neccesary) 
      length_of = length_of - 1
      neccesary.pop(length_of)
      for ready1 in neccesary:
        requeststring = requeststring + ready1
      requeststring = requeststring + ")"
      cursor.execute(requeststring)
      connection1.commit()
      connection1.close()
      cursor.close()
    except ConnectionError as e:
      msg = e
      all_msg = msg + " : connection denied"
      return all_msg

#update data in database
#meanings of arguments this function
#tablename - name of neccesary table
#newvalues - dictionary, where key - column name, value - new value of this column
#conditioncolumn - column name, that we use for condition
#conditionvalue - value of conditioncolumn
  def update (self, tablename, newvalues, conditioncolumn, conditionvalue):
     try:
      connection1 = usedb.connect(self.server, self.user, self.password, self.db)
      cursor = connection1.cursor()
      requeststring = "UPDATE "+tablename+" SET "
      items = ""
      for key in newvalues:
        items = items+" "+key+"= '"+newvalues[key]+"' ,"
      neccesary = items.split()
      length_of = len(neccesary) 
      length_of = length_of - 1
      neccesary.pop(length_of)
      for ready1 in neccesary:
        requeststring = requeststring + ready1
      requeststring = requeststring + " " + "WHERE " + conditioncolumn + " = '" + conditionvalue +"'"
      cursor.execute(requeststring)
      connection1.commit()
      connection1.close()
      cursor.close()
     except ConnectionError as e:
      msg = e
      all_msg = msg + " : connection denied"
      return all_msg

#delete data in database
#meanings of arguments this function
#tablename - name of neccesary table
#conditioncolumn - column name, that we use for condition
#conditionvalue - value of conditioncolumn
  def drop (self, tablename, conditioncolumn, conditionvalue):
     try:
      connection1 = usedb.connect(self.server, self.user, self.password, self.db)
      cursor = connection1.cursor() 
      requeststring = "DELETE FROM "+tablename+" WHERE "+conditioncolumn+"= '"+conditionvalue+"'"
      cursor.execute(requeststring)
      connection1.commit()
      connection1.close()
      cursor.close()
     except ConnectionError as e:
      msg = e
      all_msg = msg + " : connection denied"
      return all_msg

